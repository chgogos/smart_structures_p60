%----------------------------------------------------------------------------
% EXAMPLE: Clamped Laminated Composite Plate 
%   OF LAM (1998)
%  
%------------------------------------
%  input data for control parameters
%------------------------------------
clear all;
format long e

global  ELXY %Stiff1
global IPDF IPDR  %Point
global SF GDSF SFH GDSFH GDDSFH %Shape
global A B D E A44 A45 A55
global P0 P1 P2
global COOR ID ELEM MESH MATE CONT nac alpha

MATE=in_MATE;
MESH=in_MESH;
COOR=in_COOR_Lam;
[ID,ELEM]=in_ELEM_Lam;
CONT=in_CONT;
nac=CONTi(CONT,1);
alpha=CONTi(CONT,2);
% V=CONTi(CONT,3);
%------------------------
 [L,V,W0,F]=s1_W(COOR,ID,ELEM,MATE,MESH,CONT);
 
%=================================
fname='yy';
title_s='xx';
figure
W_graph_LP(COOR,ELEM,alpha,W0, fname, title_s)
