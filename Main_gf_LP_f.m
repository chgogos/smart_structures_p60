function [cost, xOpt] = Main_gf_LP_f(population_size, generations, eliteCount, filename)

global  ELXY %Stiff1
global IPDF IPDR  %Point
global SF GDSF SFH GDSFH GDDSFH %Shape
global A B D E A44 A45 A55
global P0 P1 P2
global COOR ID ELEM MESH MATE CONT nac alpha;
% COOR=in_COOR;
% [ID,ELEM]=in_ELEM;
% MESH=in_MESH;
% MATE=in_MATE;
% CONT=in_CONT;
% nac=CONTi(CONT,1);
% alpha=CONTi(CONT,2);
% V=CONTi(CONT,3);


% sol=[alpha' V'];
% sum = s1_W_gf(sol);
% fprintf('The sum of absolute dispositions is %f\n', sum);

% rng(1821, 'twister');
population_generated_GD = 2;
% lower_voltage=-100;
% upper_voltage=100;
intCon=1:36;
% Clamped-free Beam
% LB=[zeros(1,36) lower_voltage*ones(1,nac)];
% UB=[ones(1,36) upper_voltage*ones(1,nac)];
LB=[zeros(1,36) ];
UB=[ones(1,36)];
% matrix = generate_population_LP(population_generated_GD, nac, alpha, V, lower_voltage, upper_voltage);
row1=[ones(1,36)];
row2=[ones(1,36)*-1];
a=[row1;row2];
b=[nac;-nac];
fitnessFunction = @s1_W_gf;
numberOfVariables =36;
opts=gaoptimset;
opts=gaoptimset(opts, 'PopulationSize', population_size, 'Generations', generations, 'TolFun', 1e-25);
% opts=gaoptimset(opts, 'InitialPopulation', matrix);
opts=gaoptimset(opts, 'EliteCount', eliteCount);
opts=gaoptimset(opts, 'OutputFcns', @gogos_output_fcn);
[xOpt fval, ~]=ga(fitnessFunction,numberOfVariables,a,b,[],[],...
    LB,UB,[],intCon,opts);
% disp(xOpt);
% cost = log(abs(1/(fval)));
cost =fval;
fprintf('fitness cost of solution is %f\n', cost);
title_s1 = sprintf('G=%d P=%d E=%d C=%e V=[', generations, population_size, eliteCount, cost);
title_s2=  sprintf('%.2f ', xOpt);
title_s = strcat(title_s1, title_s2, ']');
%
% drawCF_LP(xOpt, nac, COOR, ID, ELEM, MATE, MESH, CONT, filename, title_s);
