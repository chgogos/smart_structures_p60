function [xdot ugout]=ssClosed(t,x)
 
global A_red Bu_red Bf_red C_red num_of_modes_total nac Fmod 
global G 

% State space equations
F=zeros(num_of_modes_total,1);

u=zeros(nac,1);

dt=1e-3; 
% F=zeros(num_of_modes_total,1);
if t>=2 && t<=2+dt
  F=Fmod;
else
   F=zeros(num_of_modes_total,1); 
end

u=-G*x;

% Output u for graphics
ugout=u;
    
% State-space linear equations
xdot=A_red*x+Bu_red*F+Bf_red*u;

 
