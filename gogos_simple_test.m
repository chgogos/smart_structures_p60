clear all;
format short e;

global MATE MESH COOR ID ELEM CONT nac;

MATE=in_MATE;
MESH=in_MESH;
COOR=in_COOR_Lam;
[ID,ELEM]=in_ELEM_Lam;
CONT=in_CONT;

numberElements=36;
nac=10;
rng(1821, 'twister');
tic;
ITER = 100;
min = bitmax
for k=1:ITER
    alpha=zeros(numberElements,1);
    tmp = randperm(numberElements);
    for i=tmp(1:nac)
        alpha(i)=1;
    end
    %     for i=datasample(1:numberElements, nac, 'Replace', false)
    %         alpha(i)=1;
    %     end
    f1 = s1_W_gf(alpha');
    if (f1<min)
        min = f1;
        best = alpha';
        fprintf('Fitness evaluations per second = %.2f\n', toc/k);
        fprintf('Controllability index CI=%e solution=%s \n', min, num2str(find(best==1)));
    end
end
fprintf('Fitness evaluations per second = %.2f\n', toc/ITER);
fprintf('Controllability index CI=%e solution=%s \n', min, num2str(find(best==1)));