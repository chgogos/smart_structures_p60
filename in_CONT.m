function CONT=in_CONT


nac=10;
alpha=zeros(36,1);
% for i=7:7+(nac-1)     
%     alpha(i)=1;
% end

for i=1:nac
    V(i)=50;
end
V=V';

CONT=[1e50; nac; 1e50;
    2e50; alpha; 2e50;
    3e50; V; 3e50];

