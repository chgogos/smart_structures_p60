function [B]=KinematicMatrix

%--------------------------------------------------------------------------
%  Purpose:
%     determine the kinematic matrix expression relating bending curvatures 
%     to rotations and displacements for shear deformable plate bending
%
%  Synopsis:
%     [kinmtpb]=fekinepb(nnel,dhdx,dhdy) 
%
%  Variable Description:
%     nnel - number of nodes per element
%     dhdx - derivatives of shape functions with respect to x   
%     dhdy - derivatives of shape functions with respect to y
%--------------------------------------------------------------------------
global SF GDSF SFH GDSFH GDDSFH %Shape

B=zeros(8,20);

 for i=1:4
         i1=(i-1)*5+1;  
         i2=i1+1;
         i3=i2+1;
         i4=i3+1;
         i5=i4+1;
         dhdx(i)=GDSF(1,i);
         dhdy(i)=GDSF(2,i);
         B(1,i1)=dhdx(i);
         B(2,i2)=dhdy(i);
         B(3,i1)=dhdy(i);
         B(3,i2)=dhdx(i);
         B(4,i4)=-dhdx(i);
         B(5,i5)=-dhdy(i);
         B(6,i4)=-dhdy(i);
         B(6,i5)=-dhdx(i);
         B(7,i3)=dhdy(i);
         B(7,i5)= -SF(i);
         B(8,i3)=dhdx(i);
         B(8,i4)= -SF(i);
 
 end
