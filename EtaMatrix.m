function [H]=EtaMatrix

global SF GDSF SFH GDSFH GDDSFH %Sh
%--------------------------------------------------------------------------
% 

 for i=1:4
     i1=(i-1)*5+1;  
     i2=i1+1;
     i3=i2+1;
     i4=i3+1;
     i5=i4+1;
     H(1,i1)=SF(i);
     H(2,i2)=SF(i);
     H(3,i3)=SF(i);
     H(4,i4)=SF(i);
     H(5,i5)=SF(i);
 end
