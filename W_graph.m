function W_graph(COOR,ELEM,L,W)
% Plot the deflection of the plate for different examples and save in a file.
% load g_data;
% COOR=in_COOR;
% [ID,ELEM]=in_ELEM;
% MATE=in_MATE;
% CONT=in_CONT;
IEN=ELEM(1:4,:);
LM=ELEM(5:24,:);
LMAC=ELEM(25,:);
% Wd=CONTi(CONT,8);
% W0=CONTi(CONT,9);
nnp=length(W);
a=max(max(COOR(:,1 )))* 1E3;
b=max(max(COOR(:,2)))*1E3;
c1 =min(W)* 1E3;
c0=W((nnp+1)/2)* 1E3;
c2=max(W)* 1E3;
ax=[0 b];
by=[0 a];
cz=[0 5];
[ned nel]=size(LM);
addra=find(L~=0);
Wmax=max(W)* 1E3;
Wmin=min(W)* 1E3;
Wctr=W((nnp+ 1)/2)* 1E3;
addrmax=find(Wmax==W* 1E3);
addrmin=find(Wmin==W* 1E3);
Xmax=COOR(addrmax, 1)* 1E3;
Ymax=COOR(addrmax,2)* 1E3;
Xmin=COOR(addrmin, 1)* 1E3;
Ymin=COOR(addrmin,2)* 1E3;
Xctr=COOR((nnp+1)/2,1)* 1E3;
Yctr=COOR((nnp+1)/2,2)* 1E3;

orient tall;
colormap('gray');
% subplot(1 ,1 ,1);
set(gca,'fontsize',8,'fontangle', 'oblique', 'xtick' ,ax, 'ytick' ,by, 'ztick',cz,...
    'defaulttextfontsize',8,'defaulttextfontangle','oblique') 
text(30,200,20,...
    'Fig.5 Simulation of the Plate Deformation', ...
        'fontsize',10);
text(30,200,19.5,...
        'Under Input Voltages of All Sets of Actuators',...
        'fontsize',10);
text(30,200,18.5,...
    '(T300/976, [0/�45�]s, clamped -free-free-free)',...
        'fontsize',10);
% text(250,0,15,'Optimal Input Voltages (V)');
% text(230,0,12, '120.0 ----- -14.7 ----- 104.5');
% text(230,0,10.5,'120.0 ----- 120.0 120.0 120.0');
% text(230,0,9, '120.0 ----- ----- 91.8');
% text([50;50],[220;220],[2;-3], ['+50�F';'-50�F']);
% text(372+5,115,-1,'x (mm)');
% text(186,228+22,-1,'y (mm)');
% text(-10,228,15,'z (mm)');
% text(Ymax,Xmax,Wmax+0.5, 'wmax=+0.648');
% text(Ymin,Xmin,abs(Wmin)+0.5,'Wmin=-0.728');
% text(Yctr,Xctr,abs(Wctr)+0.5,'Wctr=-0.724');
hold on;
for i=1:nel
lnd=IEN(:,i);
X=COOR(lnd,1)*1E3;
x=[X;X(1)];
Y=COOR(lnd,2)*1E3;
y=[Y;Y(1)];
Z=W(lnd)*1E3;
z=[Z;Z(1 )];
% Zd=Wd(lnd)* 1E3;
% zd=[Zd;Zd(1)];
% Z0=W0(lnd)* 1E3;
% z0=[Z0;Z0(1)];
    if LMAC(i)-addra~=0
        %line(y,x,z);
        fill3(y,x,z,[0.9 0.9 0.9]);         % the numbers specify the color
        %line(y,x,zd);fill3(y,x,zd,'y');
        %line(y,x,z0);fiI13(y,x,z0,'c');
        line(y,x);
    else
        %line(y,x,z);
        fill3(y,x,z,[0.8 0.8 0.8]);
        %line(y,x,zd);fill3(y,x,zd,'b');
        %line(y,x,z0);fill3(y,x,z0,'r');
        line(y,x);
    end
    %   place element number
    midx1=mean(x(1:4));
    midy1=mean(y(1:4));
    text(midy1,midx1,num2str(i),'fontsize',8);
end
hold off;
axis('ij');   %places the coordinate system origin in the upper left corner
% axis([0 b (-b+a) a 0 15]);
view(-80,90-37.5);
%print -dmeta fig50.wmf
