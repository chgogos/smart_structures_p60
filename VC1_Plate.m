%----------------------------------------------------------------------------
% EXAMPLE: Clamped Laminated Composite Plate 
%   OF LAM (1998)
%  
%------------------------------------
%  input data for control parameters
%------------------------------------
clear all;
format long e

global  ELXY Q0 QX QY %Stiff1
global IPDF IPDR  %Point
% global P0 P1 P2  %Integ
global  TN TM  %Therm
global SF GDSF SFH GDSFH GDDSFH %Shape
% global A B D  A44 A45 A55 %Stiff2
global PIEZO_E PM PN
global A B D E A44 A45 A55
global P0 P1 P2
global A_red Bu_red Bf_red num_of_modes_total num_of_modes_used  nac nse
global  G a1 a2 gamma1

lx=0.2;
ly=0.2;
ndof=5;    %number of degrees of freedom per node
numberDof=ndof;
numberElementsX=6;
numberElementsY=6;
numberElements=numberElementsX*numberElementsY;
%

ITYPE=2;
numberNodes=4;                    % number of nodes per element =4 for ITYPE>1, =8 for ITYPE==1
nnel=numberNodes;                    % number of nodes per element =4
NPE=nnel;
NDF=ndof;

% if NPE<=4
% IEL=1;
% else
% IEL=2;
% end
%
%========INPUT ======
AK=5/6;   %shear coefficient

% r_c=1;
t_ply=0.00025;
hcore=4*t_ply;
Q0=-100;
QX=0;
QY=0;
TN=0;
TM=0;
PIEZO_E=zeros(numberElements,1);
PM=zeros(20,1);
PN=zeros(20,1);

%=============================
%    Mesh generation
% =============================

COOR=in_COOR_Lam;
xx=COOR(:,1);
yy=COOR(:,2);

numberNodes=size(xx,1);     % total number of nodes = nnode
GDof=numberDof*numberNodes; % GDof: global number of degrees of freedom=5*24=125
GLXY=COOR;
%===================================
[ID,ELEM]=in_ELEM_Lam;
%------------------------
IEN=ELEM(1:4,:);
LM=ELEM(5:24,:);
LMAC=ELEM(25,:);
%[nee,nel]= size(LM);
neq=max(max(ID));
%----------------------------------
elementNodes=IEN';
%-----------------------------------
nac=10;
alpha=zeros(numberElements,1);
for i=7:7+9     
    alpha(i)=1;
end
% nac=numberElements;
% alpha=ones(nac,1);
nse=nac;
lac=find(alpha~=0);
V=-200*ones(nac,1);

%----------------------------
% plot discretization
%------------------------
% figure
% Mesh_graph_xy(COOR,ELEM,alpha);
%===========================================
%  Calculation of Matrerial matrices for CORE
%=========================================
t=[0.00025;0.00025;0.00025;0.00025];
th=[-45;45;-45;45];
E1=[150e9;150e9;150e9;150e9];
E2=[9e9;9e9;9e9;9e9];
G12=[7.1e9;7.1e9;7.1e9;7.1e9];
G13=[7.1e9;7.1e9;7.1e9;7.1e9];
G23=[2.5e9;2.5e9;2.5e9;2.5e9];
v12=[0.3;0.3;0.3;0.3];
v21=v12;
r=[1.60e3;1.60e3;1.60e3;1.60e3];

%
[A,B,D,E]=Laminate_D(th,t,E1,E2,G12,G13,G23,v12,v21,AK);
A44=E(1,1);
A45=E(1,2);
A55=E(2,2);

[P0, P1, P2]=Laminate_I(r, t);

%==============================================
%  Calculation of Matrerial matrices for PIEzOELECTRICS
%=========================================
t_p=[0.1e-3];
th_p=[0];
E1_p=[63.0e9];
E2_p=[63.0e9];
G12_p=[24.2e9];
G13_p=[24.2e9];
G23_p=[24.2e9];
v12_p=[0.3];
v21_p=v12_p;
r_p=[7600];
ract=r_p;
rsens=r_p;

Qtop=[E1_p/(1-v12_p*v21_p)             v12_p*E2_p/(1-v12_p*v21_p)   0; ...
          v12_p*E2_p/(1-v12_p*v21_p)        E2_p/(1-v12_p*v21_p)          0;
          0                        0                  G12_p];
 Qbot=Qtop;
 Qtop_s=AK*[G23_p       0;
                          0          G13_p];
  Qbot_s=Qtop_s;


Q11_p=Qtop(1,1);
Q12_p=Qtop(1,2);
Q22_p=Qtop(2,2);
Q44_p=Qtop_s(1,1);
Q55_p=Qtop_s(2,2);

d31=254e-12;
d32=254e-12;
d24=584e-12;
d15=584e-12;
ksi11=15.3*10^(-9);
ksi22=ksi11;
ksi33=15.0*10^(-9);
e31_p=d31*Q11_p+d32*Q12_p;
e32_p=d31*Q12_p+d32*Q22_p;
e15_p=d15*Q55_p;
e24_p=d24*Q44_p;
e31_a=e31_p;
e31_s=e31_p;
e32_a=e32_p;
e32_s=e32_p;
ksi33_a=ksi33;
ksi33_s=ksi33;
hsens=t_p;
hact=t_p;
%===========================================
hc=hcore;
Atop=Qtop*hact;
Abot=Qbot*hsens;
Btop=(1/2)*Qtop*((hact+hc/2)^2-(hc/2)^2);
Bbot=(1/2)*Qbot*((-hc/2)^2-(-hsens-hc/2)^2);
Dtop=(1/3)*Qtop*((hact+hc/2)^3-(hc/2)^3);
Dbot=(1/3)*Qbot*((-hc/2)^3-(-hsens-hc/2)^3);
Etop=Qtop_s*hact;
Ebot=Qbot_s*hsens;
A44top=Etop(1,1);
A45top=Etop(1,2);
A55top=Etop(2,2);
A44bot=Ebot(1,1);
A45bot=Ebot(1,2);
A55bot=Ebot(2,2);
%------------------------------------
P0top=ract*hact;
P1top=(1/2)*ract*(hact^2+hc*hact);
P2top=ract*((hc/2)^2*hact+(hc/2)*hact^2+(1/3)*hact^3);
P0bot=rsens*hsens;
P1bot=-(1/2)*rsens*(hsens^2+hc*hsens);
P2bot=rsens*((hc/2)^2*hsens+(hc/2)*hsens^2+(1/3)*hsens^3);
%----------------------------------------------
%  initialization of matrices and vectors for stiffness and mass
%----------------------------------------------
ff=zeros(neq,1);        % system force vector
kk=zeros(neq);            % system force vector
mm=zeros(neq);          % system mass matrix 
Kuf_a=zeros(neq,nac);
Kuf_s=zeros(neq,nse);
Kff_s=zeros(nse,nse);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
IPDF=2;
IPDR=1; 
%
for iel=1:numberElements          % loop for the total number of elements
    addr= find(LMAC(iel)==lac);
    acn=sum(alpha(1:LMAC(iel)));
    
     for i=1:nnel
            nd(i)=elementNodes(iel,i);         % extract connected node for (iel)-th element
            ELXY(i,1)=COOR(nd(i),1);  % extract x value of the node
            ELXY(i,2)=COOR(nd(i),2);  % extract y value of the node
     end
     fm=ELF_mine(lx,ly,2,NPE,NDF,IPDF, 1,Q0, QX, QY);
%             
            if addr~=0
                [Dlam]=mtrx_Del( hact, hsens, hcore, Qtop,Qbot,Qtop_s,Qbot_s);
                [I_Lam]=mtrx_Iel( hact, hsens, hcore, ract, rsens) ;              
                [kuu,m]=ELKM_mine( NPE, NDF,IPDF, IPDR, Dlam, I_Lam);
                [kuf_s,kuf_a, kff_s, kff_a ]=ELKuf_FSDT(NPE, IPDF, e31_s, e32_s,e31_a, e32_a, ksi33_s, ksi33_a,hsens, hact, hcore);
            else
                [Dlam]=mtrx_Del( hact, hsens, hcore, zeros(3),zeros(3),zeros(2),zeros(2));
                [I_Lam]=mtrx_Iel( hact, hsens, hcore, 0,0) ;              
                [kuu,m]=ELKM_mine( NPE, NDF,IPDF, IPDR, Dlam,I_Lam);
                kuf_a=zeros(20,1);
                kuf_s=zeros(20,1);
                kff_s=0;
            end
%----------------------------------------------
%  Assemble
%----------------------------------------------
      [DOF,dof]=sort(LM(:,iel));
        for i=1:20
            if DOF(i)~=0
                for j=1:20
                    if DOF(j)~=0 && kuu(dof(i), dof(j))~=0
                       kk(DOF(i),DOF(j))=kk(DOF(i),DOF(j))+kuu(dof(i),dof(j));
                    end
                end
                 if addr~=0
                     Kuf_a(DOF(i),acn)=Kuf_a(DOF(i),acn)+kuf_a(dof(i));
                     Kuf_s(DOF(i),acn)=Kuf_s(DOF(i),acn)+kuf_s(dof(i));
                     Kff_s(acn,acn)=kff_s;
                 end
                    ff(DOF(i))=ff(DOF(i))+fm(dof(i));
            end 
        end
        %----------------------------------------------
        %  Assemble Mass Matrix
        %----------------------------------------------
        [DOF,dof]=sort(LM(:,iel));
        for i=1:20
            if DOF(i)~=0
                for j=1:20
                    if DOF(j)~=0 && m(dof(i), dof(j))~=0
                        mm(DOF(i),DOF(j))=mm(DOF(i),DOF(j))+m(dof(i),dof(j));
                    end
                end
            end
        end
 end            %end of loop for iel
%
%  force0=ff-Kuf_a*V0*ones(nac,1)+Kuf_s*V0*ones(nac,1);
%  force1=ff-Kuf_a*V1*ones(nac,1)+Kuf_s*V1*ones(nac,1);
%  force2=ff-Kuf_a*V2*ones(nac,1)+Kuf_s*V2*ones(nac,1);
%-----------------------------
%==========================
%%%----sensor equ matrix----%%%%

Ksensor=Kff_s\Kuf_s';

%----------------------------
%  solve the matrix equation
%----------------------------
Fm0=zeros(neq,1);
Fm0(118,1)=-1;
U=kk\Fm0;
%------------------------------------
K=kk;
M=mm;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------
%Plot Mode Shapes (G.Stavroulakis)
%----------------------------------
[Phi,wn]=eig(K,M);

wn=diag(sqrt(wn));
[wn,ii] = sort(wn);
Phi=Phi(:,ii);
f=wn/(2*pi);
f(1:5);
% % 
%=============================================
disp0=U;
% disp0=Phi(:,1);
%=============================================
% Due to the orthogonality of the eigenvectors 
% (phi_j'*M*phi_i=phi_j'*K*phi_i=0),the eigenvector matrix Phi is such that
% Phi'*M*Phi and Phi'*K*Phi are diagonal. We normalize the eigenvector 
% matrix Phi to Phibar=Phi*inv(sqrt(Phi'*M*Phi)), 
% such that Phibar'*M*Phibar is the identity matrix and 
% Phibar'*Ki*Phibar=diag(wi^2). Since Phi'*M*Phi is diagonal,
% inv(sqrt(Phi'*M*Phi)) means that we find the square root and then we 
% inverse the diagonal elements of Phi'*M*Phi. This is achieved in 
% NormFactor. Note that diag() of a square matrix returns its
% diagonal as a vector and diag() of a vector returns a square matrix
% having the vector in its main diagonal
% Normalization
NormFactor=diag(1./sqrt(diag(Phi'*M*Phi)));
Phibar=Phi*NormFactor;

% How many modes to be used
num_of_modes_used=12;

% Indexes of truncated modes used
index_reduced=1:num_of_modes_used;

% Phibar_red contains the first flexural modes of the normalized Phibar
Phibar_red = Phibar(:,index_reduced);

% The following lines both calculate the reduced vector of the eigenvalues (not
% needed in the sequel)
% D_red = D(index_reduced);
% D_red = sqrt(diag(Phibar_red'*K*Phibar_red));

% Define the total number of degrees of freedom and the total number of 
% modes from the size of the modal matrix
[num_of_dof_total,num_of_modes_total] = size(Phi);

% Damping ratios all modes (values should decrease!!!)
zeta=0.008*ones(num_of_modes_total,1);
% Damping ratios for four modes
zeta_red=0.008*ones(num_of_modes_used,1);

% Matrices in modal coordinates
% Mass matrix in modal coordinates, should be equal to the identity matrix
% (not needed in the sequel)
Mmod=Phibar'*M*Phibar;
% Stifness matrix in modal coordinates, should be the diagonal matrix  
% of the eigenvalues squared
Omega=Phibar'*K*Phibar;
% Global mechanical force in modal coordinates
Fmod=Phibar'*Fm0;
% To verify it is -Phibar'*Kfua' in the equations
Kufmod=Phibar'*Kuf_a;
% Lambda=diag(2*zetai*wi)
Lambda=diag(2*sqrt(diag(Omega)).*zeta);

% Truncated matrices in modal coordinates
% Truncated mass matrix in modal coordinates, should be equal to the 
% identity matrix (not needed in the sequel)
Mmod_red=Phibar_red'*M*Phibar_red;
% Truncated stifness matrix in modal coordinates, should be the truncated diagonal 
% matrix of the eigenvalues squared
Omega_red=Phibar_red'*K*Phibar_red;
% Truncated global mechanical force in modal coordinates
Fmod_red=Phibar_red'*Fm0;
% To verify it is -Phibar_red'*Kfua' in the equations
Kufmod_red=Phibar_red'*Kuf_a;
% Lambda=diag(2*zetai*wi)
Lambda_red=diag(2*sqrt(diag(Omega_red)).*zeta_red);

% Matrices of the state space equations, all modes included 
Ax=[zeros(num_of_modes_total),eye(num_of_modes_total);
    -Omega,-Lambda];

Bu=[zeros(num_of_modes_total,num_of_modes_total);
    Phibar'];

Bf=[zeros(num_of_modes_total,nac);
      Kufmod];
  
C=[Ksensor*Phibar zeros(nse,num_of_modes_total);
    zeros(nse,num_of_modes_total) Ksensor*Phibar];

% Truncated matrices of the state space equations
A_red=[zeros(num_of_modes_used),eye(num_of_modes_used);
    -Omega_red,-Lambda_red];

Bu_red=[zeros(num_of_modes_used,num_of_modes_total);
     Phibar_red'];

Bf_red=[zeros(num_of_modes_used,nac);
      Kufmod_red];     

C_red=[Ksensor*Phibar_red zeros(nse,num_of_modes_used);
    zeros(nse,num_of_modes_used) Ksensor*Phibar_red];

%=======================================================
%--Controlability index-----
S=svd(Bf_red);
CI=1/prod(S);     

%===Objective Function =======
f1=CI;
%=============================================
