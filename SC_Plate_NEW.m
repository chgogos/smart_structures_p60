%----------------------------------------------------------------------------
% EXAMPLE: Clamped Laminated Composite Plate 
%   OF LAM (1998)
%  
%------------------------------------
%  input data for control parameters
%------------------------------------
clear all;
format long e

global  ELXY %Stiff1
global IPDF IPDR  %Point
global SF GDSF SFH GDSFH GDDSFH %Shape
global A B D E A44 A45 A55
global P0 P1 P2


lx=0.2;
ly=0.2;
ndof=5;    %number of degrees of freedom per node
numberDof=ndof;
numberElementsX=6;
numberElementsY=6;
numberElements=numberElementsX*numberElementsY;
%

numberNodes=4;                    % number of nodes per element =4 for ITYPE>1, =8 for ITYPE==1
nnel=numberNodes;                    % number of nodes per element =4
NPE=nnel;
NDF=ndof;

%========INPUT ======
AK=5/6;   %shear coefficient

% r_c=1;
t_ply=0.00025;
hcore=4*t_ply;

%=============================
%    Mesh generation
% =============================

COOR=in_COOR_Lam;
xx=COOR(:,1);
yy=COOR(:,2);

numberNodes=size(xx,1);     % total number of nodes = nnode
GDof=numberDof*numberNodes; % GDof: global number of degrees of freedom=5*24=125
GLXY=COOR;
%===================================
[ID,ELEM]=in_ELEM_Lam;
%------------------------
IEN=ELEM(1:4,:);
LM=ELEM(5:24,:);
LMAC=ELEM(25,:);
%[nee,nel]= size(LM);
neq=max(max(ID));
%----------------------------------
elementNodes=IEN';
%-----------------------------------
nac=10;
alpha=zeros(numberElements,1);
for i=7:7+9     
    alpha(i)=1;
end
% nac=numberElements;
% alpha=ones(nac,1);
nse=nac;
lac=find(alpha~=0);
for i=1:nac
    V(i)=0;
end
V=V';
%----------------------------
% plot discretization
%------------------------
figure
Mesh_graph_xy(COOR,ELEM,alpha);
%===========================================
%  Calculation of Matrerial matrices for CORE
%=========================================
t=[0.00025;0.00025;0.00025;0.00025];
th=[-45;45;-45;45];
E1=[150e9;150e9;150e9;150e9];
E2=[9e9;9e9;9e9;9e9];
G12=[7.1e9;7.1e9;7.1e9;7.1e9];
G13=[7.1e9;7.1e9;7.1e9;7.1e9];
G23=[2.5e9;2.5e9;2.5e9;2.5e9];
v12=[0.3;0.3;0.3;0.3];
v21=v12;
r=[1.60e3;1.60e3;1.60e3;1.60e3];

%
[A,B,D,E]=Laminate_D(th,t,E1,E2,G12,G13,G23,v12,v21,AK);
A44=E(1,1);
A45=E(1,2);
A55=E(2,2);

[P0, P1, P2]=Laminate_I(r, t);

%==============================================
%  Calculation of Matrerial matrices for PIEzOELECTRICS
%=========================================
t_p=[0.1e-3];
th_p=[0];
E1_p=[63.0e9];
E2_p=[63.0e9];
G12_p=[24.2e9];
G13_p=[24.2e9];
G23_p=[24.2e9];
v12_p=[0.3];
v21_p=v12_p;
r_p=[7600];
ract=r_p;
rsens=r_p;

Qtop=[E1_p/(1-v12_p*v21_p)             v12_p*E2_p/(1-v12_p*v21_p)   0; ...
          v12_p*E2_p/(1-v12_p*v21_p)        E2_p/(1-v12_p*v21_p)          0;
          0                        0                  G12_p];
 Qbot=Qtop;
 Qtop_s=AK*[G23_p       0;
                          0          G13_p];
  Qbot_s=Qtop_s;


Q11_p=Qtop(1,1);
Q12_p=Qtop(1,2);
Q22_p=Qtop(2,2);
Q44_p=Qtop_s(1,1);
Q55_p=Qtop_s(2,2);

d31=254e-12;
d32=254e-12;
d24=584e-12;
d15=584e-12;
ksi11=15.3*10^(-9);
ksi22=ksi11;
ksi33=15.0*10^(-9);
e31_p=d31*Q11_p+d32*Q12_p;
e32_p=d31*Q12_p+d32*Q22_p;
e15_p=d15*Q55_p;
e24_p=d24*Q44_p;
e31_a=e31_p;
e31_s=e31_p;
e32_a=e32_p;
e32_s=e32_p;
ksi33_a=ksi33;
ksi33_s=ksi33;
hsens=t_p;
hact=t_p;
%===========================================
%----------------------------------------------
%  initialization of matrices and vectors for stiffness and mass
%----------------------------------------------
kk=zeros(neq);            % system force vector
mm=zeros(neq);          % system mass matrix 
Kuf_a=zeros(neq,nac);
Kuf_s=zeros(neq,nse);
Kff_s=zeros(nse,nse);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
IPDF=2;
IPDR=1; 
%
for iel=1:numberElements          % loop for the total number of elements
    addr= find(LMAC(iel)==lac);
    acn=sum(alpha(1:LMAC(iel)));
    
     for i=1:nnel
            nd(i)=elementNodes(iel,i);         % extract connected node for (iel)-th element
            ELXY(i,1)=COOR(nd(i),1);  % extract x value of the node
            ELXY(i,2)=COOR(nd(i),2);  % extract y value of the node
     end
%             
            if addr~=0
                [Dlam]=mtrx_Del( hact, hsens, hcore, Qtop,Qbot,Qtop_s,Qbot_s);
                [I_Lam]=mtrx_Iel( hact, hsens, hcore, ract, rsens) ;              
                [kuu,m]=ELKM_mine( NPE, NDF,IPDF, IPDR, Dlam, I_Lam);
                [kuf_s,kuf_a, kff_s, kff_a ]=ELKuf_FSDT(NPE, IPDF, e31_s, e32_s,e31_a, e32_a, ksi33_s, ksi33_a,hsens, hact, hcore);
            else
                [Dlam]=mtrx_Del( hact, hsens, hcore, zeros(3),zeros(3),zeros(2),zeros(2));
                [I_Lam]=mtrx_Iel( hact, hsens, hcore, 0,0) ;              
                [kuu,m]=ELKM_mine( NPE, NDF,IPDF, IPDR, Dlam,I_Lam);
                kuf_a=zeros(20,1);
                kuf_s=zeros(20,1);
                kff_s=0;
            end
%----------------------------------------------
%  Assemble
%----------------------------------------------
      [DOF,dof]=sort(LM(:,iel));
        for i=1:20
            if DOF(i)~=0
                for j=1:20
                    if DOF(j)~=0 && kuu(dof(i), dof(j))~=0
                       kk(DOF(i),DOF(j))=kk(DOF(i),DOF(j))+kuu(dof(i),dof(j));
                    end
                end
                 if addr~=0
                     Kuf_a(DOF(i),acn)=Kuf_a(DOF(i),acn)+kuf_a(dof(i));
                     Kuf_s(DOF(i),acn)=Kuf_s(DOF(i),acn)+kuf_s(dof(i));
                     Kff_s(acn,acn)=kff_s;
                 end
             end 
        end
        %----------------------------------------------
        %  Assemble Mass Matrix
        %----------------------------------------------
        [DOF,dof]=sort(LM(:,iel));
        for i=1:20
            if DOF(i)~=0
                for j=1:20
                    if DOF(j)~=0 && m(dof(i), dof(j))~=0
                        mm(DOF(i),DOF(j))=mm(DOF(i),DOF(j))+m(dof(i),dof(j));
                    end
                end
            end
        end
 end            %end of loop for iel
%
%==========================
%%%----sensor equ matrix----%%%%

% Ksensor=Kff_s\Kuf_s';

%----------------------------
%  solve the matrix equation
%----------------------------
Fm0=zeros(neq,1);
Fm0(118,1)=-1;
%-----------------------------
 force=Fm0+Kuf_a*V-Kuf_s*V;
U=kk\force;
%-==================================
% Objective Function
%=========================
W0=U(3:5:end);
sum=0;
for i=1:length(W0)
    sum =sum + abs(W0(i));
end
%=================================
W=[0;U(3:5:30);0;U(33:5:60);0;U(63:5:90);0;U(93:5:120);...
    0;U(123:5:150);0;U(153:5:180);0;U(183:5:210)];

figure
W_graph(COOR,ELEM,alpha,W)
