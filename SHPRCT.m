%% SHaPe functions for ReCTangular elements
function [DET]=SHPRCT(NPE,XI,ETA,ELXY,ITYPE)
global SF GDSF SFH GDSFH GDDSFH %Shape
global P0 P1 P2 P3 P4 P5 P6 %Integ
XNODE =[-1.0 1.0 1.0 -1.0 0.0 1.0 0.0 -1.0 0.0 ; -1.0 -1.0 1.0 1.0 -1.0 0.0 1.0 0.0 0.0];
XNODE=XNODE';
NP=[1 2 3 4 5 7 6 8 9];
%FNC = A*B;
if NPE==4
% LINEAR Lagrange interpolation functions for FOUR-NODE element
for I = 1:NPE
        XP = XNODE(I,1);
        YP = XNODE(I,2);
        XI0 = 1.0+XI*XP;
        ETA0=1.0+ETA*YP;
        SF(I) = 0.25*XI0*ETA0;
        DSF(1,I)= 0.25*XP*ETA0;
        DSF(2,I)= 0.25*YP*XI0;
end
else
if NPE==8
% QUADRATIC Lagrange interpol. funct. for EIGHT-NODE element(NPE=8)
%
for I = 1:NPE
NI = NP(I);
XP = XNODE(NI,1);
YP = XNODE(NI,2);
XI0 = 1.0+XI*XP;
ETA0 = 1.0+ETA*YP;
XI1 = 1.0-XI*XI;
ETA1 = 1.0-ETA*ETA;
if I<=4
SF(NI) = 0.25*XI0*ETA0*(XI*XP+ETA*YP-1.0);
DSF(1,NI) = 0.25*ETA0*XP*(2.0*XI*XP+ETA*YP);
DSF(2,NI) = 0.25*XI0*YP*(2.0*ETA*YP+XI*XP);
else
if I<=6
SF(NI) = 0.5*XI1*ETA0;
DSF(1,NI) = -XI*ETA0;
DSF(2,NI) = 0.5*YP*XI1;
else
SF(NI) = 0.5*ETA1*XI0;
DSF(1,NI) = 0.5*XP*ETA1;
DSF(2,NI) = -ETA*XI0;
end
end
end
else
%
% QUADRATIC Lagrange interpol. funct. for NINE-NODE element (NPE=9)
%
for I=1:NPE
NI = NP(I);
XP = XNODE(NI,1);
YP = XNODE(NI,2);
XI0 = 1.0+XI*XP;
ETA0 = 1.0+ETA*YP;
XI1 = 1.0-XI*XI;
ETA1 = 1.0-ETA*ETA;
XI2 = XP*XI;
ETA2 = YP*ETA;
if I<=4
SF(NI) = 0.25*XI0*ETA0*XI2*ETA2;
DSF(1,NI)= 0.25*XP*ETA2*ETA0*(1.0+2.0*XI2);
DSF(2,NI)= 0.25*YP*XI2*XI0*(1.0+2.0*ETA2);
else
if I <= 6
SF(NI) = 0.5*XI1*ETA0*ETA2;
DSF(1,NI) = -XI*ETA2*ETA0;
DSF(2,NI) = 0.5*XI1*YP*(1.0+2.0*ETA2);
else
if I <= 8
SF(NI) = 0.5*ETA1*XI0*XI2;
DSF(2,NI) = -ETA*XI2*XI0;
DSF(1,NI) = 0.5*ETA1*XP*(1.0+2.0*XI2);
else
SF(NI) = XI1*ETA1;
DSF(1,NI) = -2.0*XI*ETA1;
DSF(2,NI) = -2.0*ETA*XI1;
end
end
end
end %for
end
end
%
% Compute the Jacobian matrix [GJ] and its inverse [GJINV]
%
for I = 1:2
    for J = 1:2
        GJ(I,J) = 0.0;
        for K = 1:NPE
            GJ(I,J) = GJ(I,J) + DSF(I,K)*ELXY(K,J);
        end
    end
end
%
DET = GJ(1,1)*GJ(2,2)-GJ(1,2)*GJ(2,1);
GJINV(1,1) = GJ(2,2)/DET;
GJINV(2,2) = GJ(1,1)/DET;
GJINV(1,2) = -GJ(1,2)/DET;
GJINV(2,1) = -GJ(2,1)/DET;
%
% Compute the derivatives of the interpolation functions with
% respect to the global coordinates (x,y): [GDSF]
%
    for I = 1:2
        for J = 1:NPE
            GDSF(I,J) = 0.0;
            for K = 1:2
                GDSF(I,J) = GDSF(I,J) + GJINV(I,K)*DSF(K,J);
            end
        end
    end
if ITYPE<=1
if ITYPE==1
NET=4*NPE;
II = 1;
for I = 1:NPE
XP = XNODE(I,1);
YP = XNODE(I,2);
XI1 = XI*XP-1.0;
XI2 = XI1-1.0;
ETA1 = ETA*YP-1.0;
ETA2 = ETA1-1.0;
XI0 = (XI+XP)^2;
ETA0 = (ETA+YP)^2;
XIP0 = XI+XP;
XIP1 = 3.0*XI*XP+XP*XP;
XIP2 = 3.0*XI*XP+2.0*XP*XP;
YIP0 = ETA+YP;
YIP1 = 3.0*ETA*YP+YP*YP;
YIP2 = 3.0*ETA*YP+2.0*YP*YP;
%
SFH(II) = 0.0625*ETA0*ETA2*XI0*XI2;
DSFH(1,II) = 0.0625*ETA0*ETA2*XIP0*(XIP1-4.0);
DSFH(2,II) = 0.0625*XI0*XI2*YIP0*(YIP1-4.0);
DDSFH(1,II) = 0.125*ETA0*ETA2*(XIP2-2.0);
DDSFH(2,II) = 0.125*XI0*XI2*(YIP2-2.0);
DDSFH(3,II) = 0.0625*(XIP1-4.0)*(YIP1-4.0)*XIP0*YIP0;
%
SFH(II+1) = -0.0625*XP*XI0*XI1*ETA0*ETA2;
DSFH(1,II+1) = -0.0625*ETA0*ETA2*XP*XIP0*(XIP1-2.0);
DSFH(2,II+1) = -0.0625*XI0*XI1*XP*YIP0*(YIP1-4.);
DDSFH(1,II+1) = -0.125*ETA0*ETA2*XP*(XIP2-1.0);
DDSFH(2,II+1) = -0.125*XI0*XI1*(YIP2-2.0)*XP;
DDSFH(3,II+1) = -0.0625*XP*XIP0*(XIP1-2.)*(YIP1-4.)*YIP0;
%
SFH(II+2) = -0.0625*YP*XI0*XI2*(ETA0*ETA1);
DSFH(1,II+2) = -0.0625*ETA0*ETA1*YP*XIP0*(XIP1-4.);
DSFH(2,II+2) = -0.0625*XI0*XI2*YP*YIP0*(YIP1-2.);
DDSFH(1,II+2) = -0.125*ETA0*ETA1*YP*(XIP2-2.);
DDSFH(2,II+2) = -0.125*XI0*XI2*YP*(YIP2-1.0);
DDSFH(3,II+2) = -0.0625*YP*YIP0*(YIP1-2.)*(XIP1-4.0)*XIP0;
%
SFH(II+3) = 0.0625*XP*YP*XI0*XI1*(ETA0*ETA1);
DSFH(1,II+3) = 0.0625*ETA0*ETA1*XP*YP*(XIP1-2.)*XIP0;
DSFH(2,II+3) = 0.0625*XI0*XI1*XP*YP*(YIP1-2.)*YIP0;
DDSFH(1,II+3) = 0.125*ETA0*ETA1*XP*YP*(XIP2-1.);
DDSFH(2,II+3) = 0.125*XI0*XI1*XP*YP*(YIP2-1.0);
DDSFH(3,II+3) = 0.0625*XP*YP*YIP0*XIP0*(YIP1-2.)*(XIP1-2.);
II = I*4 + 1;
end
else
NET=3*NPE;
II = 1;
for I = 1:NPE
XP = XNODE(I,1);
YP = XNODE(I,2);
XI0 = XI*XP;
ETA0 = ETA*YP;
XIP1 = XI0+1;
ETAP1 = ETA0+1;
XIM1 = XI0-1;
ETAM1 = ETA0-1;
XID = 3.0+2.0*XI0+ETA0-3.0*XI*XI-ETA*ETA-2.0*XI/XP;
ETAD = 3.0+XI0+2.0*ETA0-XI*XI-3.0*ETA*ETA-2.0*ETA/YP;
ETAXI = 4.0+2.0*(XI0+ETA0)-3.0*(XI*XI+ETA*ETA)- 2.0*(ETA/YP+XI/XP);
%
SFH(II) = 0.125*XIP1*ETAP1*(2.0+XI0+ETA0-XI*XI-ETA*ETA);
DSFH(1,II) = 0.125*XP*ETAP1*XID;
DSFH(2,II) = 0.125*YP*XIP1*ETAD;
DDSFH(1,II) = 0.250*XP*ETAP1*(XP-3.0*XI-1.0/XP);
DDSFH(2,II) = 0.250*YP*XIP1*(YP-3.0*ETA-1.0/YP);
DDSFH(3,II) = 0.125*XP*YP*ETAXI;
%
SFH(II+1) = 0.125*XP*XIP1*XIP1*XIM1*ETAP1;
DSFH(1,II+1) = 0.125*XP*XP*ETAP1*(3.0*XI0-1.0)*XIP1;
DSFH(2,II+1) = 0.125*XP*YP*XIP1*XIP1*XIM1;
DDSFH(1,II+1) = 0.250*XP*XP*XP*ETAP1*(3.0*XI0+1.0);
DDSFH(2,II+1) = 0.0;
DDSFH(3,II+1) = 0.125*XP*XP*YP*(3.0*XI0-1.0)*XIP1;
%
SFH(II+2) = 0.125*YP*XIP1*ETAP1*ETAP1*ETAM1;
DSFH(1,II+2) = 0.125*XP*YP*ETAP1*ETAP1*ETAM1;
DSFH(2,II+2) = 0.125*YP*YP*XIP1*(3.0*ETA0-1.0)*ETAP1;
DDSFH(1,II+2) = 0.0;
DDSFH(2,II+2) = 0.250*YP*YP*YP*XIP1*(3.0*ETA0+1.0);
DDSFH(3,II+2) = 0.125*XP*YP*YP*(3.0*ETA0-1.0)*ETAP1;
II = I*3 + 1;
end
end
DDSF(1,1) = 0.0;
DDSF(2,1) = 0.0;
DDSF(3,1) = 0.250;
DDSF(1,2) = 0.0;
DDSF(2,2) = 0.0;
DDSF(3,2) = - 0.250;
DDSF(1,3) = 0.0;
DDSF(2,3) = 0.0;
DDSF(3,3) = 0.250;
DDSF(1,4) = 0.0;
DDSF(2,4) = 0.0;
DDSF(3,4) = - 0.250;

for I = 1:2
for J = 1:NET
SUM = 0.0;
for K = 1:2
SUM = SUM + GJINV(I,K)*DSFH(K,J);
end
GDSFH(I,J) = SUM;
end
end
for I = 1:3
for J = 1:2
SUM = 0.0D0;
for K = 1:NPE
SUM = SUM + DDSF(I,K)*ELXY(K,J);
end
DJCB(I,J) = SUM;
end
end
%
for K = 1:3
for J = 1:NET
SUM = 0.0;
for L = 1:2
SUM = SUM + DJCB(K,L)*GDSFH(L,J);
end
DDSJ(K,J) =SUM;
end
end
%
%
% Compute the jacobian of the transformation
%
GGJ(1,1)=GJ(1,1)*GJ(1,1);
GGJ(1,2)=GJ(1,2)*GJ(1,2);
GGJ(1,3)=2.0*GJ(1,1)*GJ(1,2);
GGJ(2,1)=GJ(2,1)*GJ(2,1);
GGJ(2,2)=GJ(2,2)*GJ(2,2);
GGJ(2,3)=2.0*GJ(2,1)*GJ(2,2);
GGJ(3,1)=GJ(2,1)*GJ(1,1);
GGJ(3,2)=GJ(2,2)*GJ(1,2);
GGJ(3,3)=GJ(2,1)*GJ(1,2)+GJ(1,1)*GJ(2,2);
GGINV=inv(GGJ); % CALL INVRSE(GGJ,GGINV);
%
for I = 1:3
for J = 1:NET
SUM = 0.0;
for K = 1:3
SUM = SUM + GGINV(I,K)*(DDSFH(K,J)-DDSJ(K,J));
end
GDDSFH(I,J) = SUM;
end
end
end
