function [A,B,C,E]=Laminate_D(th,t,E1,E2,G12,G13,G23,v12,v21,k_sh)
% -------------------------------------------------------
% Purpose: Calculate the Dlam matrix for the laminated Composite Layer

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Initializing A,B,C,E %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


A=zeros(3,3);B=zeros(3,3);C=zeros(3,3);E=zeros(2,2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Setting the (h) Matrix  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

h=zeros(size(t,1)+1,1);
h(1,1)=sum(t)/2;
for i=2:size(t,1)+1
    h(i)=h(i-1)-t(i-1);
end
h;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Calculating (Qb_bar)'s, (A,B,C & E) Matrices %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

k=1;
for i=size(t,1):-1:1
    
    Qb_k =[E1(i,1)/(1-v12(i,1)*v21(i,1))             v12(i,1)*E2(i,1)/(1-v12(i,1)*v21(i,1))   0; ...
          v12(i,1)*E2(i,1)/(1-v12(i,1)*v21(i,1))       E2(i,1)/(1-v12(i,1)*v21(i,1))          0;
          0                        0                  G12(i,1)];
   
    eval(['Qb_bar' int2str(k) '= T_sig(th(' int2str(i) '))*Qb_k*T_eps(th(' int2str(i) '));']);
    Qb_bar=eval(['Qb_bar' int2str(k) ';']);
    
    Qs_k =k_sh*[G23(i,1)       0;
                          0          G13(i,1)];

    eval(['Qs_bar' int2str(k) '= Ts_sig(th(' int2str(i) '))*Qs_k*Ts_eps(th(' int2str(i) '));']);
    Qs_bar=eval(['Qs_bar' int2str(k) ';']);
    
    A = A - Qb_bar * (h(i+1,1) - h(i,1));                 % Negative summation since i=size(t,1):-1:1
    B = B - 1/2 * Qb_bar * (h(i+1,1)^2 - h(i,1)^2);
    C = C - 1/3 * Qb_bar * (h(i+1,1)^3 - h(i,1)^3);
    E = E - Qs_bar * (h(i+1,1) - h(i,1));
    
      
    k=k+1;
end
A44=E(1,1);
A55=E(2,2);
A45=E(1,2);


% --------------------------------
%       INTERNAL FUNCTIONS
% --------------------------------

function T_sig=T_sig(ang)

th=ang*pi/180;
m=cos(th);
n=sin(th);

T_sig=[ ...
        m^2 n^2 -2*m*n
        n^2 m^2 2*m*n
        m*n -m*n m^2-n^2];
    
function T_eps=T_eps(ang)

th=ang*pi/180;
m=cos(th);
n=sin(th);

T_eps=[ ...
        m^2 n^2 m*n
        n^2 m^2 -m*n
        -2*m*n 2*m*n m^2-n^2];

function Ts_sig=Ts_sig(ang)

th=ang*pi/180;
m=cos(th);
n=sin(th);

Ts_sig=[ ...
        m n
        -n m];
        
 function Ts_eps=Ts_eps(ang)

th=ang*pi/180;
m=cos(th);
n=sin(th);

Ts_eps=[ ...
        m -n
        n m];