close all;
clear all;
clc;
format long e

global  ELXY %Stiff1
global IPDF IPDR  %Point
global SF GDSF SFH GDSFH GDDSFH %Shape
global A B D E A44 A45 A55
global P0 P1 P2
global A_red Bu_red Bf_red num_of_modes_total num_of_modes_used nse
global COOR ID ELEM MESH MATE CONT nac alpha msg;

% generations = 1000;
% populations_list = [50, 100];
% eliteCount_list = [0,2,4];
generations = 1000;
populations_list = 100;
eliteCount_list = 2;

formatOut = 'yyyy-mm-dd HH-MM-SS';
str = datestr(now,formatOut,'local');
fName = strcat('results-nac10-W-', str, '.txt');
fid = fopen(fName,'w');
fprintf(fid, 'Results collected on %s\n', str);
fclose(fid);
c=1;
for j = 1:numel(populations_list)
    population = populations_list(j);
    for k=1:numel(eliteCount_list)
        tic;
       
        MATE=in_MATE;
        MESH=in_MESH;
        COOR=in_COOR_Lam;
        [ID,ELEM]=in_ELEM_Lam;
        CONT=in_CONT;
        nac=CONTi(CONT,1);
        alpha=CONTi(CONT,2);
         
            
        eliteCount = eliteCount_list(k);
        msg='';
        fid = fopen(fName,'a');
        fName2 = strcat('results-nac10-CI', str, '-', int2str(c), '.png');
        [cost, xOpt] = Main_gf_LP_f(population, generations, eliteCount, fName2);
        elapsed_time = toc;
        fprintf(fid, '%d) Generations=%d Population=%d EliteCount=%d Cost=%e (%s)Elapsed Time=%.1f Solution =[', ... 
                c, generations, population, eliteCount, cost, msg, elapsed_time);
        fprintf(fid, '%.4f ', xOpt);
        fprintf(fid, ']\n');
        fclose(fid);
        c=c+1;
    end;
end;
fid = fopen(fName,'a');
str = datestr(now,formatOut,'local');
fprintf(fid, 'Finished at %s\n', str);
fclose(fid);