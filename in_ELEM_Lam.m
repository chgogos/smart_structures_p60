function [ID,ELEM]=in_ELEM_Lam

numberElementsX=6;
numberElementsY=6;
NX=numberElementsX;
NY=numberElementsY;
%-------------------------------
% Clamped-Free Plate of LAM (1998)
%
ID=[0;0;0;0;0];
  for i=1:NX
      ID=[ID [1;2;3;4;5]+(i-1)*5];
  end
  ID=[ID [0;0;0;0;0]];
  for i=NX+2:2*NX+1
      ID=[ID [31;32;33;34;35]+(i-(NX+2))*5];
  end
   ID=[ID [0;0;0;0;0]];
  for i=2*NX+2:3*NX+1
      ID=[ID [61;62;63;64;65]+(i-(2*NX+2))*5];
  end
  ID=[ID [0;0;0;0;0]];
  for i=3*NX+2:4*NX+1
      ID=[ID [91;92;93;94;95]+(i-(3*NX+2))*5];
  end
  ID=[ID [0;0;0;0;0]];
  for i=4*NX+2:5*NX+1
      ID=[ID [121;122;123;124;125]+(i-(4*NX+2))*5];
  end
  ID=[ID [0;0;0;0;0]];
  for i=5*NX+2:6*NX+1
      ID=[ID [151;152;153;154;155]+(i-(5*NX+2))*5];
  end
  ID=[ID [0;0;0;0;0]];
  for i=6*NX+2:7*NX+1
      ID=[ID [181;182;183;184;185]+(i-(6*NX+2))*5];
  end
  
% -------------------------------
nelements=numberElementsX*numberElementsY;
%=========================
cnct = zeros(nelements,4);
iel=0;
for i=1:numberElementsX
    for j=1:numberElementsY
        iel=(j-1)*numberElementsX+i;
            cnct(iel,1)=(j-1)*(numberElementsX+1)+i;
            cnct(iel,2)=(j-1)*(numberElementsX+1)+i+1;
            cnct(iel,4)=j*(numberElementsX+1)+i;
            cnct(iel,3)=j*(numberElementsX+1)+i+1;
    end
end
% copy connectivity in matrnumberElementsX elementNodes
elementNodes=cnct;
IEN=elementNodes';

LM=[];
for i=1:nelements
    LMi=[ID(:,IEN(1,i));ID(:,IEN(2,i));ID(:,IEN(3,i));ID(:,IEN(4,i))];
    LM=[LM LMi];
end
% b1=[1 1 1 1 1 1 2 2 2 2 2 2 3 3 3 3 3 3 4 4 4 4 4 4 5 5 5 5 5 5];
b1=[1:nelements];
LMAC=b1;
ELEM=[IEN;LM;LMAC];