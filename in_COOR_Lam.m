function COOR=in_COOR_Lam
% The nodal coordinates for the example whose deflection has been
% calculated by Lam et al
lx=0.2;
ly=0.2;
numberElementsX=6;
numberElementsY=6;
% automatic numbering of a composite sandwitch plate
% automatic preparation of a rectangular area
% length and number of segments in x direction
nelementNodes = (numberElementsX+1)*(numberElementsY+1); 
% number of layers
% nlayers = 3;
% total number of elementNodes
    nnode = nelementNodes;
% number of elements for each layer
nelements = numberElementsX*numberElementsY;
% total number of elements
    nel = nelements;
% elementNodes preparation of x and y coordinates
xnode=zeros(nnode,1);
ynode=zeros(nnode,1);
iel=0;
for j=1:numberElementsY+1
    for i=1:numberElementsX+1
        for iel=iel+1;
            xnode(iel)=(lx/numberElementsX)*(i-1);
            ynode(iel)=(ly/numberElementsY)*(j-1);
            xnode(iel+nelementNodes) = (lx/numberElementsX)*(i-1);
            ynode(iel+nelementNodes) = (ly/numberElementsY)*(j-1);
        end
    end
end
% copy results in matrix gcoord
nodeCoordinates = zeros(nnode,2);
for i=1:nnode
    nodeCoordinates(i,1) = xnode(i);
    nodeCoordinates(i,2) = ynode(i);
end
COOR=nodeCoordinates;