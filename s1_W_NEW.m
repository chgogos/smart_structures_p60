function [W]=s1_W_NEW(COOR,ID,ELEM,MATE,MESH,CONT,V)


IEN=ELEM(1:4,:);
LM=ELEM(5:24,:);
[ndof,nnp]= size(ID);
[nee,nel]= size(LM);
neq=max(max(ID));
[Wm,C]=mtrx_C(COOR,ID,ELEM,MATE,MESH,CONT);
Wel=C*V;
W=Wm+Wel;
