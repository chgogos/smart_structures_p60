function [ matrix ] = generate_population_LP( pop, nac, alpha)
matrix = [];
for k=1:pop
    pfi=0.001;
    iterations = 500;
    c1=0;
    c2=0;
    % Step 1. Generate an initial solution
    sol = alpha;
    % Step 2. Calculate initial cost F(S) and set Level L=F(S)
    fs = s1_W_gf(sol);
    L = fs;
    fs_best=fs;
    sol_best=sol;
    fsf=0;
    decay_rate = (fs-fsf)/iterations;
    % Step 3.
    stepc=randi([100,200]);
    fprintf('Solution %d will be created [Decay rate=%e stepc=%.2f]\n',k, decay_rate, stepc);
    for i=0:iterations-1
        % Step 3.1 Employ neighborhood generation function to generate neighbor
        stepc=stepc - exp(-mod(i/(i+1),1))*pfi*stepc;
        %         if mod(i,100)==0
        %             fprintf('stepc =%.15f Level=%.15f accept better=%d accept worse=%d\n',stepc, L, c1, c2);
        %         end
        sol_new = getNeighbor(sol, nac, stepc);
        fs_new = s1_W_gf(sol_new);
        % Step 3.2
        if (fs_new <= fs)
            c1=c1+1;
            sol=sol_new;
            fs = fs_new;
            if (fs<fs_best)
                fs_best = fs;
                sol_best=sol;
                fprintf('Better solution found %e\n', fs_best);
            end
        elseif (fs_new <= L)
            c2=c2+1;
            sol=sol_new;
            fs = fs_new;
        end
        % Step 3.3
        L = L - decay_rate;
    end
    fprintf('fitness cost of solution is %e\n\n', fs_best)
    matrix = [matrix;sol_best];
end
end

