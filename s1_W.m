function [L,V,W,F]=s1_W(COOR,ID,ELEM,MATE,MESH,CONT)

nac=CONTi(CONT,1);
L=CONTi(CONT,2);
V=CONTi(CONT,3);

IEN=ELEM(1:4,:);
LM=ELEM(5:24,:);
[ndof,nnp]= size(ID);
[nee,nel]= size(LM);
neq=max(max(ID));
[Wm,C]=mtrx_C(COOR,ID,ELEM,MATE,MESH,CONT);
Wel=C*V;
W=Wm+Wel;
Q=eye(nnp);
F=(W)'*Q*(W);