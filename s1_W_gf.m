function f1 = s1_W_gf(sol)

global  ELXY %Stiff1
global IPDF IPDR  %Point
global SF GDSF SFH GDSFH GDDSFH %Shape
global A B D E A44 A45 A55
global P0 P1 P2
global A_red Bu_red Bf_red num_of_modes_total num_of_modes_used nse
global COOR ID ELEM MESH MATE CONT nac;
%-------------------------------
c=0;
for i=1:36
    c = c+ sol(i);
end
if (c~=nac)
    f1 = 1e9;
else
%---------------------
for i=1:36
    CONT(4+i)=sol(i);
end
%
% for i=1:nac
%     V(i)=sol(36+i);
% end
% V=V';
% for i=1:nac
%     CONT(41+i)=V(i);
% end

%===========================================
[nee,nnp]= size(ID);
npn=find(ID(3,:)~=0);
%[n,nel]=size(ELEM);
neq=max(max(ID));

[kk,mm, Kuf_a, Kuf_s, Fm0]=mtrx_GL(COOR,ID,ELEM,MATE,MESH,CONT);


U=kk\Fm0;
%====================================
K=kk;
M=mm;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------
%Plot Mode Shapes (G.Stavroulakis)
%----------------------------------
[Phi,wn]=eig(K,M);
wn=diag(sqrt(wn));
[wn,ii] = sort(wn);
Phi=Phi(:,ii);
f=wn/(2*pi);
f(1:5);
%=============================================
disp0=U;
% disp0=Phi(:,1);
%=============================================
% Due to the orthogonality of the eigenvectors 
% (phi_j'*M*phi_i=phi_j'*K*phi_i=0),the eigenvector matrix Phi is such that
% Phi'*M*Phi and Phi'*K*Phi are diagonal. We normalize the eigenvector 
% matrix Phi to Phibar=Phi*inv(sqrt(Phi'*M*Phi)), 
% such that Phibar'*M*Phibar is the identity matrix and 
% Phibar'*Ki*Phibar=diag(wi^2). Since Phi'*M*Phi is diagonal,
% inv(sqrt(Phi'*M*Phi)) means that we find the square root and then we 
% inverse the diagonal elements of Phi'*M*Phi. This is achieved in 
% NormFactor. Note that diag() of a square matrix returns its
% diagonal as a vector and diag() of a vector returns a square matrix
% having the vector in its main diagonal
% Normalization
NormFactor=diag(1./sqrt(diag(Phi'*M*Phi)));
Phibar=Phi*NormFactor;

% How many modes to be used
num_of_modes_used=12;

% Indexes of truncated modes used
index_reduced=1:num_of_modes_used;

% Phibar_red contains the first flexural modes of the normalized Phibar
Phibar_red = Phibar(:,index_reduced);

% The following lines both calculate the reduced vector of the eigenvalues (not
% needed in the sequel)
% D_red = D(index_reduced);
% D_red = sqrt(diag(Phibar_red'*K*Phibar_red));

% Define the total number of degrees of freedom and the total number of 
% modes from the size of the modal matrix
[num_of_dof_total,num_of_modes_total] = size(Phi);

% Damping ratios all modes (values should decrease!!!)
zeta=0.008*ones(num_of_modes_total,1);
% Damping ratios for four modes
zeta_red=0.008*ones(num_of_modes_used,1);

% Matrices in modal coordinates
% Mass matrix in modal coordinates, should be equal to the identity matrix
% (not needed in the sequel)
Mmod=Phibar'*M*Phibar;
% Stifness matrix in modal coordinates, should be the diagonal matrix  
% of the eigenvalues squared
Omega=Phibar'*K*Phibar;
% Global mechanical force in modal coordinates
Fmod=Phibar'*Fm0;
% To verify it is -Phibar'*Kfua' in the equations
Kufmod=Phibar'*Kuf_a;
% Lambda=diag(2*zetai*wi)
Lambda=diag(2*sqrt(diag(Omega)).*zeta);

% Truncated matrices in modal coordinates
% Truncated mass matrix in modal coordinates, should be equal to the 
% identity matrix (not needed in the sequel)
Mmod_red=Phibar_red'*M*Phibar_red;
% Truncated stifness matrix in modal coordinates, should be the truncated diagonal 
% matrix of the eigenvalues squared
Omega_red=Phibar_red'*K*Phibar_red;
% Truncated global mechanical force in modal coordinates
Fmod_red=Phibar_red'*Fm0;
% To verify it is -Phibar_red'*Kfua' in the equations
Kufmod_red=Phibar_red'*Kuf_a;
% Lambda=diag(2*zetai*wi)
Lambda_red=diag(2*sqrt(diag(Omega_red)).*zeta_red);


% Truncated matrices of the state space equations
% A_red=[zeros(num_of_modes_used),eye(num_of_modes_used);
%     -Omega_red,-Lambda_red];
% 
% Bu_red=[zeros(num_of_modes_used,num_of_modes_total);
%      Phibar_red'];

Bf_red=[zeros(num_of_modes_used,nac);
      Kufmod_red];     

% C_red=[Ksensor*Phibar_red zeros(nse,num_of_modes_used);
%     zeros(nse,num_of_modes_used) Ksensor*Phibar_red];

%=======================================================
%--Controlability index-----
S=svd(Bf_red);
CI=1/prod(S);     

%===Objective Function =======
f1=CI;
%=============================================
end
