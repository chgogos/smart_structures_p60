function[ELF ]=ELF_mine(XL,YL,LPT,NPE,NDF,IPDF, LOAD,Q0, QX, QY)


global SF GDSF SFH GDSFH GDDSFH %Shape
global ELXY


GAUSPT=[0.0 0.0 0.0 0.0 0.0;
                    -0.57735027 0.57735027 0.0 0.0 0.0;
                    -0.77459667 0.0 0.77459667 0.0 0.0;
                    -0.86113631 -0.33998104 0.33998104 0.86113631 0.0;
                    -0.90617984 -0.53846931 0.0 0.53846931 0.9061798];
GAUSPT=GAUSPT';
%
GAUSWT=[2.0 0.0 0.0 0.0 0.0;
                    1.0 1.0 0.0 0.0 0.0;
                    0.55555555 0.88888888 0.55555555 0.0 0.0;
                    0.34785485 0.65214515 0.65214515 0.34785485 0.0;
                    0.23692688 0.47862867 0.56888888 0.47862867 0.23692688];
GAUSWT=GAUSWT';
%

ELF=zeros(20,1);
% Do-loops on numerical (Gauss) integration begin here. Subroutine
% SHPRCT (SHaPe functions for ReCTangular elements) is called here
for NI = 1:IPDF
    for NJ = 1:IPDF
            XI = GAUSPT(NI,IPDF);
            ETA = GAUSPT(NJ,IPDF);
    %
                [DET]=SHPRCT(NPE,XI,ETA,ELXY,2);
    %
            CNST = DET*GAUSWT(NI,IPDF)*GAUSWT(NJ,IPDF);
            X=0.0;
            Y=0.0;
            for I=1:NPE
                    X=X+ELXY(I,1)*SF(I);
                    Y=Y+ELXY(I,2)*SF(I);
            end
            H=EtaMatrix;                      
            ELF=ELF+H'*[0;0;Q0;0;0]*CNST;
            
%             Hf=EtaMatrix_F;                      
%             ELF=ELF+Hf'*Q0*CNST;
%                 II=1;
%                 for I=1:NPE
%                     II1=II+1;
%                     II2=II+2;
%                     II3=II+3;
%                     II4=II+4;
%                      ELF(II2)=ELF(II2)+CNST*SF(I)*Q0;
%                     ELF(II) =ELF(II) +CNST*SF(I)*Q0;
%                     ELF(II1)=ELF(II1)+CNST*SF(I)*Q0;
%                     ELF(II3)=ELF(II3)+CNST*SF(I)*Q0;
%                     ELF(II4)=ELF(II4)+CNST*SF(I)*Q0;
% 
%                     II = NDF*I+1;
%                 end    
    end
end
% ELF=1/4*ELF;
%

