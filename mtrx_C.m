function [Wm,C] =mtrx_C(COOR,ID,ELEM,MATE,MESH,CONT)

global  ELXY %Stiff1
global IPDF IPDR  %Point
global SF GDSF SFH GDSFH GDDSFH %Shape
global A B D E A44 A45 A55
global P0 P1 P2%nee: 

[nee,nnp]= size(ID);
npn=find(ID(3,:)~=0);
%[n,nel]=size(ELEM);
neq=max(max(ID));

[K,mm, Kuf_a, Kuf_s, Fm0]=mtrx_GL(COOR,ID,ELEM,MATE,MESH,CONT);
Kuf=Kuf_a- Kuf_s;
SinvK=zeros(nnp,neq);
invK=inv(K);
SinvK(npn,:)=invK(ID(3,npn),:);
Wm=SinvK*Fm0;
C=SinvK*Kuf;


