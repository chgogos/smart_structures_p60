function []=drawCF_LP(xOpt,nac, COOR, ID, ELEM, MATE, MESH, CONT, fname, title_s)


for i=1:36
    CONT(4+i)=xOpt(i);
end
%
for i=1:nac
    CONT(41+i)=xOpt(36+i);
end

for i=1:nac
    V(i)=xOpt(36+i);
end
V=V';

[W]=s1_W_NEW(COOR,ID,ELEM,MATE,MESH,CONT,V);

figure
Uopt=W;
alpha=CONTi(CONT,2);
% 
W_graph_LP(COOR,ELEM,alpha,Uopt, 'figure_CF_LP.png', 'CF LOCATION PROBLEM')
W_graph_LP(COOR,ELEM,L,W, fname, title_s)

title(title_s);
export_fig(fname);
