function [I_Lam]=mtrx_Iel( htop, hbot, hc, rhotop,rhobot) 

global P0 P1 P2

I0cp=P0+rhotop*htop+rhobot*hbot;
I1cp=P1+(1/2)*rhotop*(htop^2+hc*htop)-(1/2)*rhobot*(hbot^2+hc*hbot);
I2cp=P2+rhotop*((hc/2)^2*htop+(hc/2)*htop^2+(1/3)*htop^3)+rhobot*((hc/2)^2*hbot+(hc/2)*hbot^2+(1/3)*hbot^3);

I_Lam=[I0cp 0 0 -I1cp 0; 
           0 I0cp 0 0 -I1cp;
           0 0 I0cp 0 0;
           -I1cp 0 0 I2cp 0;
            0 -I1cp 0 0 I2cp];