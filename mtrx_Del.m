			function [Dlam]=mtrx_Del( htop, hbot, hc, Qtop,Qbot,Qtop_s,Qbot_s) 

%---------------------------------------------------------------------------------------------
%     rhoact, rhobody, rhosens - mass density of the actuator, body and sensor of the 
%                                             element, (mass per unit volume)
%     hact, hbody, hsens - thickess of the actuator, body and sensor of the element
%
%     Q11act, Q11body, Q11sens, Q55act, Q55body, Q55sens - reduced elastic constants
%
%     leng -  length of the beam element
%
%     sc -    Shear coefficient
%
%%-------------------------------------------------------------------------------------------------
%
global A B D E

Acp=A+Qtop*htop+Qbot*hbot;
Bcp=B+(1/2)*Qtop*((htop+hc/2)^2-(hc/2)^2)+(1/2)*Qbot*((-hc/2)^2-(-hbot-hc/2)^2);
Dcp=D+(1/3)*Qtop*((htop+hc/2)^3-(hc/2)^3)+(1/3)*Qbot*((-hc/2)^3-(-hbot-hc/2)^3);
Ecp=E+Qtop_s*htop+Qbot_s*hbot;

Dlam=[Acp Bcp zeros(3,2); 
            Bcp Dcp zeros(3,2);
             zeros(2, 3) zeros(2, 3) Ecp];

%==================================================================
