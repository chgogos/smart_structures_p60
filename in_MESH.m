function [MESH]=in_MESH

lx=0.2;
ly=0.2;
ndof=5;    %number of degrees of freedom per node
numberElementsX=6;
numberElementsY=6;
numberNodes=4;                    % number of nodes per element =4 for ITYPE>1, =8 for ITYPE==1MESH=[lx,ly,numberElements,nlayers];

MESH=[lx, ly, ndof, numberElementsX, numberElementsY, numberNodes];