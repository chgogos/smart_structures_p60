function CONTi=CONTi(CONT,i)

if i~=3
   addr= find(CONT==i*1e50);
   CONTi=CONT(addr(1)+1:addr(2)-1);
else
   addr=find(CONT==3e50);
   CONTi=CONT(addr(1)+1:addr(2)-1);
end