function [ sol_new ] = getNeighbor( sol, nac, stepc)

numberOfElements =36;
a = zeros(1, nac);
k=1;
for i=1:numberOfElements
    if sol(i) > 0 
        a(k) = i;
        k=k+1;
        if (k>nac) 
            break;
        end;
    end;
end;

for i=1:nac
    a(i)=a(i) + round((2*rand()-1) * stepc);
end;

min_v = min(a);
max_v = max(a);

aa = zeros(1, nac);
for i=1:nac
    x = round((a(i)-min_v)/(max_v-min_v)*numberOfElements);
    while (any(aa==x))
        x = x+ 1;
        if (x>numberOfElements)
            x=1;
        end;
    end;
    aa(i)=x;
end;

aaa = zeros(1,numberOfElements);
for i=1:nac
    k = aa(i);
    aaa(k)=1;
end;

sol_new =aaa;
