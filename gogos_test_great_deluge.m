global MATE MESH COOR ID ELEM CONT nac;

tic;
MATE=in_MATE;
MESH=in_MESH;
COOR=in_COOR_Lam;
[ID,ELEM]=in_ELEM_Lam;
CONT=in_CONT;
numberElements=36;
nac=10;

alpha=zeros(numberElements,1);
for i=datasample(1:numberElements, nac, 'Replace', false)
    alpha(i)=1;
end

pop = 5;
sols = generate_population_LP(pop, nac, alpha');

min= bitmax;
for i = 1:pop
    sol = sols(i,:);
    cost = s1_W_gf(sol);
    if (cost < min)
        min = cost;
        best = sol;
    end
end
fprintf('Controllability index CI=%e solution=%s \n', f1, num2str(find(best==1)));
toc;