% Control signal in the LQR case
function u=uLQR_NEW(t,x) 

global   num_of_modes_used nac Omega_red A_red Bf_red 
global a1 a2 gamma1 G

% Qx=[a2*Omega_red  zeros(num_of_modes_used);
%                 zeros(num_of_modes_used) a1*eye(num_of_modes_used)] ;
% Qx=(Qx+Qx')/2;
% R=gamma1*eye(nac);
% [G,P]=lqr(A_red, Bf_red, Qx,R,0);
u=-G*x;