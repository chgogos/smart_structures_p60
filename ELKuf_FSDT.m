function[ELKuf_top,ELKuf_bot, ELKff_top, ELKff_bot ]=ELKuf_FSDT(NPE, IPDF, e31_top, e32_top,e31_bot, e32_bot, ksi33_top, ksi33_bot, htop,hbot,hc)


global SF GDSF SFH GDSFH GDDSFH %Shape
global ELXY 


GAUSPT=[0.0 0.0 0.0 0.0 0.0;
                    -0.57735027 0.57735027 0.0 0.0 0.0;
                    -0.77459667 0.0 0.77459667 0.0 0.0;
                    -0.86113631 -0.33998104 0.33998104 0.86113631 0.0;
                    -0.90617984 -0.53846931 0.0 0.53846931 0.9061798];
GAUSPT=GAUSPT';
%
GAUSWT=[2.0 0.0 0.0 0.0 0.0;
                    1.0 1.0 0.0 0.0 0.0;
                    0.55555555 0.88888888 0.55555555 0.0 0.0;
                    0.34785485 0.65214515 0.65214515 0.34785485 0.0;
                    0.23692688 0.47862867 0.56888888 0.47862867 0.23692688];
GAUSWT=GAUSWT';
%

ELKuf_top=zeros(20,1);
ELKuf_bot=zeros(20,1);
ELKff_top=0;
ELKff_bot=0;

% Do-loops on numerical (Gauss) integration begin here. Subroutine
% SHPRCT (SHaPe functions for ReCTangular elements) is called here
for NI = 1:IPDF
    for NJ = 1:IPDF
        XI = GAUSPT(NI,IPDF);
        ETA = GAUSPT(NJ,IPDF);
%
        [DET]=SHPRCT(NPE,XI,ETA,ELXY,2);
%
        CNST = DET*GAUSWT(NI,IPDF)*GAUSWT(NJ,IPDF);
        X=0.0;
        Y=0.0;
        for I=1:NPE
        X=X+ELXY(I,1)*SF(I);
        Y=Y+ELXY(I,2)*SF(I);
        end
       %
        B=KinematicMatrix;
  
      ELKuf_top=ELKuf_top+B'*(1/htop)*[htop*e31_top; htop* e32_top; 0;(1/2)*((htop+hc/2)^2-(hc/2)^2)*e31_top;  (1/2)*((htop+hc/2)^2-(hc/2)^2)*e32_top ;0;0; 0]*CNST;
      ELKuf_bot=ELKuf_bot+B'*(1/hbot)*[hbot*e31_bot; hbot*e32_bot; 0; (1/2)*((-hc/2)^2-(-hbot-hc/2)^2)*e31_bot; (1/2)*((-hc/2)^2-(-hbot-hc/2)^2)*e32_bot;0;0; 0]*CNST;
      ELKff_top=ELKff_top+(1/htop)*ksi33_top*CNST;
       ELKff_bot=ELKff_top+(1/hbot)*ksi33_bot*CNST;
    end
end
%
