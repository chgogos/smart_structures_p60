function [P0,P1,P2]=Laminate_I(r, t)
% -------------------------------------------------------
% Purpose: Calculate the Dlam matrix for the laminated Copmosite Layer

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Initializing A,B,C,E %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


P0=0;P1=0;P2=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Setting the (h) Matrix  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

h=zeros(size(t,1)+1,1);
h(1,1)=sum(t)/2;
for i=2:size(t,1)+1
    h(i)=h(i-1)-t(i-1);
end
h;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Calculating r_k, (A,B,C) Matrices %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

k=1;
for i=size(t,1):-1:1
    
    r_k =[r(i,1)];
    
    P0 = P0 - r_k * (h(i+1,1) - h(i,1));                 % Negative summation since i=size(t,1):-1:1
    P1= P1 - 1/2 * r_k * (h(i+1,1)^2 - h(i,1)^2);
    P2 = P2 - 1/3 * r_k * (h(i+1,1)^3 - h(i,1)^3);
   
         
    k=k+1;
end


