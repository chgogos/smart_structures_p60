function[ELK, ELM ]=ELKM_mine( NPE, NDF,IPDF, IPDR, Dlam,I_Lam)


global SF GDSF SFH GDSFH GDDSFH %Shape
global ELXY thickness


GAUSPT=[0.0 0.0 0.0 0.0 0.0;
                    -0.57735027 0.57735027 0.0 0.0 0.0;
                    -0.77459667 0.0 0.77459667 0.0 0.0;
                    -0.86113631 -0.33998104 0.33998104 0.86113631 0.0;
                    -0.90617984 -0.53846931 0.0 0.53846931 0.9061798];
GAUSPT=GAUSPT';
%
GAUSWT=[2.0 0.0 0.0 0.0 0.0;
                    1.0 1.0 0.0 0.0 0.0;
                    0.55555555 0.88888888 0.55555555 0.0 0.0;
                    0.34785485 0.65214515 0.65214515 0.34785485 0.0;
                    0.23692688 0.47862867 0.56888888 0.47862867 0.23692688];
GAUSWT=GAUSWT';
%

ELKb=zeros(20);
ELKs=zeros(20);
ELM=zeros(20);
% Do-loops on numerical (Gauss) integration begin here. Subroutine
% SHPRCT (SHaPe functions for ReCTangular elements) is called here
for NI = 1:IPDF
    for NJ = 1:IPDF
        XI = GAUSPT(NI,IPDF);
        ETA = GAUSPT(NJ,IPDF);
%
        [DET]=SHPRCT(NPE,XI,ETA,ELXY,2);
%
        CNST = DET*GAUSWT(NI,IPDF)*GAUSWT(NJ,IPDF);
        X=0.0;
        Y=0.0;
        for I=1:NPE
        X=X+ELXY(I,1)*SF(I);
        Y=Y+ELXY(I,2)*SF(I);
        end
%
       B=KinematicMatrix;
       Bb=zeros (8,20);
       Bb(1:6,:)=B(1:6,:); 
       H=EtaMatrix;
  
      ELKb=ELKb+Bb'*Dlam*Bb*CNST;
      ELM=ELM+H'*I_Lam*H*CNST;
    end
end
%
% First-order theory; stiffness coefficients for transverse shear.
% Use reduced integration to evaluate coefficients associated with
% TRANSVERSE SHEARterms:___________________________________________
%
for NI=1:IPDR
       for NJ=1:IPDR
                XI = GAUSPT(NI,IPDR);
                ETA = GAUSPT(NJ,IPDR);
                %
                [DET]=SHPRCT(NPE,XI,ETA,ELXY,2);
                %
                CNST=DET*GAUSWT(NI,IPDR)*GAUSWT(NJ,IPDR);
                %
               B=KinematicMatrix;
               Bs=zeros (8,20);
               Bs(7:8,:)=B(7:8,:); 
               ELKs=ELKs+Bs'*Dlam*Bs*CNST;
       end
end
ELK=ELKb+ELKs;