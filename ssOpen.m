function [xdot]=ssOpen(t,x)
 
global A_red Bu_red  num_of_modes_total Fmod 

F=zeros(num_of_modes_total,1);
%  State space equations
dt=1e-3; 
% F=zeros(num_of_modes_total,1);
if t>=2 & t<=2+dt
  F=Fmod;
else
   F=zeros(num_of_modes_total,1); 
end
% State-space linear equations
xdot=A_red*x+Bu_red*F;