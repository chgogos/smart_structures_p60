function Mesh_graph_xy(COOR,ELEM,L)
% Plot the deflection of the plate for different examples and save in a file.

IEN=ELEM(1:4,:);
LM=ELEM(5:24,:);
LMAC=ELEM(25,:);

a=(max(max(COOR(:,1 ))))* 1E3;
b=(max(max(COOR(:,2))))*1E3;

ax=[0 b];
by=[0 a];
cz=[0 5];
[ned nel]=size(LM);
addra=find(L~=0);

% Xmax=COOR(addrmax, 1)* 1E3;
% Ymax=COOR(addrmax,2)* 1E3;
% Xmin=COOR(addrmin, 1)* 1E3;
% Ymin=COOR(addrmin,2)* 1E3;
% Xctr=COOR((nnp+1)/2,1)* 1E3;
% Yctr=COOR((nnp+1)/2,2)* 1E3;

orient tall;
colormap('gray');

hold on;
for i=1:nel
    lnd=IEN(:,i);
    X=COOR(lnd,1)*1E3;
    x=[X;X(1)];
    Y=COOR(lnd,2)*1E3;
    y=[Y;Y(1)];

    zlength=length(X);
    Z=zeros(zlength,1)*1E3;
    z=[Z;Z(1)];

        if LMAC(i)-addra~=0
            fill3(x,y,z,[0.9 0.9 0.9]);         % the numbers specify the color
%             line(y,x);
        else
            fill3(x,y,z,[0.7 0.7 0.7]);
%             line(y,x);
        end
%   place element number
    midx1=mean(x(1:4));
    midy1=mean(y(1:4));
    text(midx1,midy1,num2str(i),'fontsize',8);
end
title('Mesh Discretization')
axis off
% put node number
% for jj=3*numberNodes+1:1:24
%   text(COOR(jj,1), COOR(jj,2), num2str(jj),'fontsize',7);
% end
hold off;
% axis('ij');
% axis off
axis([0 a (-b+a) b ]);
% axis([0 b a 0]);
% axis([0 0.16 0 0.32 0 0.15]);
% view(90-37.5,50);
%print -dmeta fig50.wmf
