global MATE MESH COOR ID ELEM CONT nac;

MATE=in_MATE;
MESH=in_MESH;
COOR=in_COOR_Lam;
[ID,ELEM]=in_ELEM_Lam;
CONT=in_CONT;

tic;
numberElements=36;
nac=10;
upperPlanePartIndexes = [31,32,33,34,35,36,25,26,27,28,29,30,...
    19,20,21,22,23,24];
min= bitmax;
combinations = combnk (1:numberElements/2,nac/2);
numberOfCombinations = size(combinations,1);
for ri = 1:numberOfCombinations
    comb = combinations(ri,:);
    alpha = zeros(numberElements,1);
    for i = comb
        alpha(i)=1;
        alpha(upperPlanePartIndexes(i))=1;
    end
    f1 = s1_W_gf(alpha');
    if (f1<min)
        min = f1;
        best = alpha';
        fprintf('Better CI=%e solution=%s iterations per second=%.2f\n', ...
            f1, num2str(find(best==1)), toc/ri);
    end
    %     fprintf('Combination(%d/%d) half solution=%s [best so far=%e]\n', ri, ...
    %         numberOfCombinations,  num2str(comb), min);
    
end

fprintf('Controllability index CI=%e solution=%s \n', best, num2str(find(best==1)));
toc;