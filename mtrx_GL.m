function [kk,mm, Kuf_a, Kuf_s, Fm0]=mtrx_GL(COOR,ID,ELEM,MATE,MESH,CONT)

global  ELXY %Stiff1
global IPDF IPDR  %Point
global SF GDSF SFH GDSFH GDDSFH %Shape
global A B D E A44 A45 A55
global P0 P1 P2


lx=MESH(1);
ly=MESH(2);
ndof=MESH(3);    %number of degrees of freedom per node
numberDof=ndof;
numberElementsX=MESH(4);
numberElementsY=MESH(5);
numberElements=numberElementsX*numberElementsY;
%
numberNodes=MESH(6);                    % number of nodes per element =4 for ITYPE>1, =8 for ITYPE==1
nnel=numberNodes;                    % number of nodes per element =4
NPE=nnel;
NDF=ndof;

%========INPUT ======
AK=5/6;   %shear coefficient

% r_c=1;
t_ply=MATE(1);
hcore=4*t_ply;

%=============================
%    Mesh generation
% =============================
xx=COOR(:,1);
yy=COOR(:,2);

numberNodes=size(xx,1);     % total number of nodes = nnode
GDof=numberDof*numberNodes; % GDof: global number of degrees of freedom=5*24=125
GLXY=COOR;
%===================================

IEN=ELEM(1:4,:);
LM=ELEM(5:24,:);
LMAC=ELEM(25,:);
%[nee,nel]= size(LM);
neq=max(max(ID));
%----------------------------------
elementNodes=IEN';
%-----------------------------------
nac=CONTi(CONT,1);
alpha=CONTi(CONT,2);
% V=CONTi(CONT,3);
lac=find(alpha~=0);
 nse=nac;
%--------------------------------------------
% nac=10;
% alpha=zeros(numberElements,1);
% for i=7:7+9     
%     alpha(i)=1;
% end
% % nac=numberElements;
% % alpha=ones(nac,1);
% nse=nac;
% lac=find(alpha~=0);
% for i=1:nac
%     V(i)=50;
% end
% V=V';
%----------------------------
%===========================================
%  Calculation of Matrerial matrices for CORE
%=========================================
tc=MATE(1);
t=[tc;tc;tc;tc];
theta=MATE(2);
th=[-theta;theta;-theta;theta];
E1c=MATE(3);
E1=[E1c;E1c;E1c;E1c];
E2c=MATE(4);
E2=[E2c;E2c;E2c;E2c];
G12c=MATE(5);
G12=[G12c;G12c;G12c;G12c];
G13c=MATE(6);
G13=[G13c;G13c;G13c;G13c];
G23c=MATE(7);
G23=[G23c;G23c;G23c;G23c];
v12c=MATE(8);
v12=[v12c;v12c;v12c;v12c];
v21=v12;
r_c=MATE(10);
r=[r_c;r_c;r_c;r_c];

%
[A,B,D,E]=Laminate_D(th,t,E1,E2,G12,G13,G23,v12,v21,AK);
A44=E(1,1);
A45=E(1,2);
A55=E(2,2);

[P0, P1, P2]=Laminate_I(r, t);

%==============================================
%  Calculation of Matrerial matrices for PIEzOELECTRICS
%=========================================
t_p=MATE(11);
th_p=MATE(12);
E1_p=MATE(13);
E2_p=MATE(14);
G12_p=MATE(15);
G13_p=MATE(16);
G23_p=MATE(17);
v12_p=MATE(18);
v21_p=MATE(19);
r_p=MATE(20);
ract=r_p;
rsens=r_p;

Qtop=[E1_p/(1-v12_p*v21_p)             v12_p*E2_p/(1-v12_p*v21_p)   0; ...
          v12_p*E2_p/(1-v12_p*v21_p)        E2_p/(1-v12_p*v21_p)          0;
          0                        0                  G12_p];
 Qbot=Qtop;
 Qtop_s=AK*[G23_p       0;
                          0          G13_p];
  Qbot_s=Qtop_s;


Q11_p=Qtop(1,1);
Q12_p=Qtop(1,2);
Q22_p=Qtop(2,2);
Q44_p=Qtop_s(1,1);
Q55_p=Qtop_s(2,2);

d31=MATE(21);
d32=MATE(22);
d24=MATE(23);
d15=MATE(24);
ksi11=MATE(25);
ksi22=MATE(26);
ksi33=MATE(27);
e31_p=d31*Q11_p+d32*Q12_p;
e32_p=d31*Q12_p+d32*Q22_p;
e15_p=d15*Q55_p;
e24_p=d24*Q44_p;
e31_a=e31_p;
e31_s=e31_p;
e32_a=e32_p;
e32_s=e32_p;
ksi33_a=ksi33;
ksi33_s=ksi33;
hsens=t_p;
hact=t_p;
%===========================================
%----------------------------------------------
%  initialization of matrices and vectors for stiffness and mass
%----------------------------------------------
kk=zeros(neq);            % system force vector
mm=zeros(neq);          % system mass matrix 
Kuf_a=zeros(neq,nac);
Kuf_s=zeros(neq,nse);
Kff_s=zeros(nse,nse);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
IPDF=2;
IPDR=1; 
%
for iel=1:numberElements          % loop for the total number of elements
    addr= find(LMAC(iel)==lac);
    acn=sum(alpha(1:LMAC(iel)));
    
     for i=1:nnel
            nd(i)=elementNodes(iel,i);         % extract connected node for (iel)-th element
            ELXY(i,1)=COOR(nd(i),1);  % extract x value of the node
            ELXY(i,2)=COOR(nd(i),2);  % extract y value of the node
     end
                
            if addr~=0
                [Dlam]=mtrx_Del( hact, hsens, hcore, Qtop,Qbot,Qtop_s,Qbot_s);
                [I_Lam]=mtrx_Iel( hact, hsens, hcore, ract, rsens) ;              
                [kuu,m]=ELKM_mine( NPE, NDF,IPDF, IPDR, Dlam, I_Lam);
                [kuf_s,kuf_a, kff_s, kff_a ]=ELKuf_FSDT(NPE, IPDF, e31_s, e32_s,e31_a, e32_a, ksi33_s, ksi33_a,hsens, hact, hcore);
            else
                [Dlam]=mtrx_Del( hact, hsens, hcore, zeros(3),zeros(3),zeros(2),zeros(2));
                [I_Lam]=mtrx_Iel( hact, hsens, hcore, 0,0) ;              
                [kuu,m]=ELKM_mine( NPE, NDF,IPDF, IPDR, Dlam,I_Lam);
                kuf_a=zeros(20,1);
                kuf_s=zeros(20,1);
                kff_s=0;
            end
%----------------------------------------------
%  Assemble
%----------------------------------------------
      [DOF,dof]=sort(LM(:,iel));
        for i=1:20
            if DOF(i)~=0
                for j=1:20
                    if DOF(j)~=0 && kuu(dof(i), dof(j))~=0
                       kk(DOF(i),DOF(j))=kk(DOF(i),DOF(j))+kuu(dof(i),dof(j));
                    end
                end
                 if addr~=0
                     Kuf_a(DOF(i),acn)=Kuf_a(DOF(i),acn)+kuf_a(dof(i));
                     Kuf_s(DOF(i),acn)=Kuf_s(DOF(i),acn)+kuf_s(dof(i));
                     Kff_s(acn,acn)=kff_s;
                 end
             end 
        end
        %----------------------------------------------
        %  Assemble Mass Matrix
        %----------------------------------------------
        [DOF,dof]=sort(LM(:,iel));
        for i=1:20
            if DOF(i)~=0
                for j=1:20
                    if DOF(j)~=0 && m(dof(i), dof(j))~=0
                        mm(DOF(i),DOF(j))=mm(DOF(i),DOF(j))+m(dof(i),dof(j));
                    end
                end
            end
        end
 end            %end of loop for iel
%
%==========================
%%%----sensor equ matrix----%%%%

% Ksensor=Kff_s\Kuf_s';

%----------------------------
%  solve the matrix equation
%----------------------------
Fm0=zeros(neq,1);
Fm0(118,1)=-1;
%-----------------------------


